import axios from "axios";
import store from "../store";

const apiPrefix = 'http://localhost:8080/api';

export function doPostRequest(url, object = {}) {
    store.commit('requestStarted');
    return axios.post(apiPrefix + url, object)
        .then(response => {
            return response.data;
        })
        .catch(error => {
            return error.response.data;
        }).then((response) => {
            if (!response.success) {
                alert(response.data.message);
            }
            store.commit('requestStopped');
            return response;
        });
}

export function doGetRequest(url, object = {}) {
    store.commit('requestStarted');
    return axios.get(apiPrefix + url, {
        params: object
    })
        .then(response => {
            return response.data;
        })
        .catch(error => {
            return error.response.data;
        }).then((response) => {
            if (!response.success) {
                alert(response.data.message);
            }
            store.commit('requestStopped');
            return response;
        });
}

export function handleUnauthenticatedResponse() {
    axios.interceptors.response.use(undefined, function (error) {
        return new Promise(function (resolve, reject) {
            if (error.response && error.response.status === 401) {
                store.commit('logout');
            }
            throw error;
        });
    });
}

export function setToken(token) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}

export function deleteToken() {
    delete axios.defaults.headers.common['Authorization'];
}