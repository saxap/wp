import Vue from 'vue'
import Vuex from 'vuex'
import {deleteToken, setToken} from "./helpers/common";
import router from "@/router"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        requestProcessing: false,
        token: localStorage.getItem('token') || null
    },
    getters: {
        isAuthenticated: state => !!state.token,
    },
    mutations: {
        requestStarted: state => {
            state.requestProcessing = true;
        },
        requestStopped: state => {
            state.requestProcessing = false;
        },
        login: (state, token) => {
            state.token = token;
            localStorage.setItem('token', token);
            setToken(token);
        },
        logout: (state) => {
            state.token = null;
            localStorage.removeItem('token');
            deleteToken();
            router.push({ path: '/' })
        }
    },
    actions: {
    }
})
