import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from "./views/Login";
import Profile from "./views/Profile";
import store from "@/store";
import NotFound from "./views/NotFound";

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return;
    }
    next('/')
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return;
    }
    next('/auth')
};

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        },
        {
            path: '/auth',
            component: Login,
            beforeEnter: ifNotAuthenticated,
        },
        {
            path: '/profile',
            component: Profile,
            beforeEnter: ifAuthenticated,
        },
        {
            path: '/logout',
            beforeEnter: ifAuthenticated,
            component: () => store.commit('logout')
        },



        {
            path: '*',
            component: NotFound
        }
    ]
})
