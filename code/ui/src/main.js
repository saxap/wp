import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import AnyField from "./components/common/AnyField";
import {handleUnauthenticatedResponse, setToken} from "./helpers/common";

Vue.config.productionTip = false;

Vue.component('AnyField', AnyField);

const token = localStorage.getItem('token');
if (token) {
    setToken(token);
}

new Vue({
    router,
    store,
    created() {
        handleUnauthenticatedResponse();
    },
    render: h => h(App)
}).$mount('#app');
