<?php

namespace App\Service;

use App\Common\BaseService;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService extends BaseService
{
    private $passwordEncoder;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function register(Request $request)
    {
        /** @var User $user */
        $user = $this->deserialize($request, User::class);

        $this->validate($user);

        $password = $this->passwordEncoder->encodePassword( $user, $user->getPassword() );
        $user->setPassword($password);
        $this->save($user);
        return $this->response($user, ['groups' => 'default']);
    }

    public function current()
    {
        $user = $this->getCurrentUser();
        return $this->response($user, ['groups' => 'default']);
    }
}