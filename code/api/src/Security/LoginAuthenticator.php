<?php

namespace App\Security;

use App\Entity\Token;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginAuthenticator extends AbstractGuardAuthenticator
{

    private $userRepository;

    private $passwordEncoder;

    private $entityManager;

    private $translator;

    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    )
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'user.login' && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $requestBody = json_decode($request->getContent(), true);

        $credentials = [
            'email' => $requestBody['email'] ?? null,
            'password' => $requestBody['password'] ?? null
        ];

        if (!$credentials['email'] || !$credentials['password'] ) {
            return false;
        }

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        return $this->userRepository->findOneBy(['email' => $credentials['email']]);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        // todo вот тут решить почему нельзя выбросить эксепшн
        $data = [
            'data' => [
                'message' => $this->translator->trans(strtr($exception->getMessageKey(), $exception->getMessageData()), [], 'security'),
            ],
            'success' => false
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $token = new Token( $token->getUser() );

        $this->entityManager->persist($token);
        $this->entityManager->flush();

        return new JsonResponse([
            'data' => [
                'token' => $token->getToken()
            ],
            'success' => true
        ]);
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        return true;
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
