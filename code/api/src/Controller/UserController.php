<?php


namespace App\Controller;

use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    /**
     * @var UserService
     */
    private $userService;

    public function __construct(
        UserService $userService
    )
    {
        $this->userService = $userService;
    }

    /**
     * @Route("/login", name="user.login")
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        return new JsonResponse( json_decode($request->getContent()) );
    }

    /**
     * @Route("/profile/current", name="current", methods={"GET"})
     */
    public function current()
    {
        return $this->userService->current();
    }

    /**
     * @Route("/profile", name="profile", methods={"GET"})
     */
    public function profile()
    {
        return new JsonResponse('Profile hello');
    }

    /**
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register(Request $request)
    {
        return $this->userService->register($request);
    }
}