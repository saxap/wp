<?php

namespace App\Common;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Exception\ExceptionInterface as ExceptionInterfaceAlias;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Security
     */
    protected $security;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @param EntityManagerInterface $entityManager
     * @required
     */
    public function setEntityManager(EntityManagerInterface $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Security $security
     * @required
     */
    public function setSecurity(Security $security): void
    {
        $this->security = $security;
    }

    /**
     * @param SerializerInterface $serializer
     * @required
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * @param ValidatorInterface $validator
     * @required
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    public function getCurrentUser()
    {
        return $this->security->getUser();
    }

    /**
     * @param $object
     * @param $params
     * @return JsonResponse
     * @throws ExceptionInterfaceAlias
     */
    public function response($object, $params)
    {
        $normalized = $this->serializer->normalize($object, 'json', $params);
        $data = [
            'success' => true,
            'data' => $normalized
        ];
        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     * @param $class
     * @param array $params
     * @return object
     */
    public function deserialize(Request $request, $class, $params = [])
    {
        $rawData = $request->getContent();
        return $this->serializer->deserialize($rawData, $class, 'json', $params);
    }

    public function validate($object)
    {
        $errors = $this->validator->validate($object);

        if (count($errors) > 0) {
            $errorsString = [];
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $errorsString[] = $error->getPropertyPath().' - '.$error->getMessage();
            }

            throw new BadRequestHttpException(implode("\n", $errorsString));
        }
    }

    public function save($object)
    {
        $this->entityManager->persist($object);
        $this->entityManager->flush();
    }
}